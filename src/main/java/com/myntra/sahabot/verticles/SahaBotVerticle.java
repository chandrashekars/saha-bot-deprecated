package com.myntra.sahabot.verticles;

import com.myntra.bot.verticles.BaseBotVerticle;
import io.vertx.core.json.JsonObject;
import myntra.vertx.common.kafka.KafkaConstants;

import java.util.Arrays;
import java.util.List;

public class SahaBotVerticle extends BaseBotVerticle {

    @Override
    public String getBotName() {
        return "saha";
    }

    @Override
    public String getBotOwnedTopic() {
        return "saha-message";
    }

    @Override
    public List<String> getListOfTopicsToBeSubscribed() {
        return Arrays.asList("customer-support-message", "recommendation-message");
    }

    @Override
    public void startMessageReceiverBus() {
        vertx.eventBus().consumer(KafkaConstants.EVENT_BUS_MESSAGE_RECEIVER, rawMessage -> {
           JsonObject jsonObject = (JsonObject) rawMessage.body();
            logger.info("SAHA Message - "+jsonObject);
        });
    }
}
